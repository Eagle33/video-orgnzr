import common.utilities as utilities
import common.constants as constants
from common.randomGen import RandomGenerator
import os

class DatabaseController():

    config = []
    previousInfo = []

    def __init__(self):
        # Load config variables
        self.config = utilities.loadOptions()

        #Get used ids from current csv info
        self.previousInfo = utilities.loadPreviousInfo()

    #Names with , will be changed
    def _validateFileNames(self, currentNames):
        videoFolderPath = self.config[constants.VIDEO_FOLDER_PATH]

        for name in currentNames:
            if ","in name:
                print("Found an invalid name...")
                newName = name.replace(",","_")
                currentPath = os.path.join(videoFolderPath, name)
                newPath = os.path.join(videoFolderPath, newName)
                print("Renaming file from '{0}' to '{1}'".format(currentPath,newPath))
                utilities.renameFile(currentPath, newPath)

        return utilities.getFileNamesFromFolder(videoFolderPath)

    def addVideos(self):

        try:
            print("Starting process ...")
            videoFolderPath = self.config[constants.VIDEO_FOLDER_PATH]
        
            #Get list of previous Ids
            previousIds = [val for val in self.previousInfo]
        
            #Read files names from folders
            fileNames = utilities.getFileNamesFromFolder(videoFolderPath)

            #Validate if names have (,) and if they do rename files accordingly
            currentFileNames = self._validateFileNames(fileNames)
           
            newFilesCount = len(currentFileNames)
            print("Found {0} files in folder {1} ...".format(str(newFilesCount),videoFolderPath))

            #Create Ids
            randomGen = RandomGenerator(previousIds)
            newIds = randomGen.generateIds(newFilesCount)
            print("Creating Ids for files ...")

            #Create video objects, assign them and ID
            #id,previousName,artist,name,genre,tags
            for index, oldFileName in enumerate(currentFileNames):
                newVideo = {}
                newVideo["id"] = newIds[index]
                newVideo["fileName"] = oldFileName
                newVideo["artist"] = ""
                newVideo["name"] =  ""
                newVideo["genre"] =  ""
                newVideo["tags"] =  ""
                newVideo["extension"] =  oldFileName.split('.')[-1]

                videoObject = {}
                videoObject[newIds[index]] = newVideo

                self.previousInfo.update(videoObject)

            #Rewrite the database
            newVideoFilePath = self.config[constants.NEW_VIDEO_FILE_PATH]
            utilities.dictionaryToCSv(self.previousInfo, newVideoFilePath)
            print("Adding new info to database...")

            #Rename files/ Relocate them
            for videoId in newIds:
                fileName = self.previousInfo[videoId]["fileName"]
                extension = "." + self.previousInfo[videoId]["extension"]
                currentPath = os.path.join(videoFolderPath, fileName)
                newPath = os.path.join(videoFolderPath, (videoId + extension))

                utilities.renameFile(currentPath, newPath)

            print("Renaming files ...")
            return True
        except Exception as e:
            print ("Error details -> " + str(e.args[0]))
            return False

        


   


