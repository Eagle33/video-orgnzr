from os import listdir, path, rename
import random, csv, json, yaml
import common.constants as constants

#Import the config 
def loadOptions():
    with open("config.yaml") as stream:
        try:
            return yaml.safe_load(stream)
        except yaml.YAMLError as exc:
            return exc

#Import previous database
def loadPreviousInfo():
    configOptions = loadOptions()
    return csvToDictionary (configOptions[constants.OLD_VIDEO_FILE_PATH], constants.MAIN_PROPERTY)

#File Utilities
def dictionaryToJson(dictionary, jsonFilePath):
    with open (jsonFilePath, "w") as jsonFile:
        jsonFile.write(json.dumps(dictionary, indent=4))

def csvToDictionary(cvsFilePath, idName):
    data = {}
    with open(cvsFilePath, encoding='utf-8-sig') as csvFile:
        csvReader = csv.DictReader(csvFile)
        for row in csvReader:
            id = row[idName]
            data[id] = row
    return data

def dictionaryToCSv(dictionary, csvFilePath):
    infoArray = []
    currentHeaders = constants.FILE_HEADERS +  "\n"
    for key, value in dictionary.items():
        infoString = ""
        index = 1
        for subKey in value.keys():
            if index == len(value.keys()):
                infoString += value[subKey] 
            else:
                infoString += value[subKey] + ","
                index = index + 1

        infoString += "\n"
        infoArray.append(infoString)

    infoArray.insert(0, currentHeaders) #Add headers on first line
    arrayToCsv(infoArray, csvFilePath)


def arrayToCsv(array, cvsFilePath):
    if len(array) == 0:
        print("No Array elements found. Won't create csv file")
        return
    file = open (cvsFilePath, 'w', encoding='utf-8')
    file.writelines(array)
    file.close()


#Folder Utilities
def getFileNamesFromFolder(folderPath):
    fileNames = [f for f in listdir(folderPath) if path.isfile(path.join(folderPath, f))]
    if len(fileNames) == 0:
        print("No elements were found on folder")
        return
    return fileNames

def renameFile(oldNameFilePath, newNameFilePath):
    rename(oldNameFilePath, newNameFilePath)
 