import random

class RandomGenerator():

    def __init__(self, previousIds):
        self.previousIds = previousIds

    CAPITAL_LETTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    NUMBERS = "0123456789"

    def createId(self):
        isUnique = True
        generatedId = ""

        while(isUnique):
            id = []
            id = id + random.choices(self.CAPITAL_LETTERS, k=4)
            id = id + random.choices(self.NUMBERS, k=4)
            random.shuffle(id)
            generatedId = ''.join(map(str, id)) 

            #Validate if it is not already assigned
            if generatedId in self.previousIds:
                isUnique = True
            else:
                isUnique = False

        return generatedId

    def generateIds(self, limit):
        ids = []
        for i in range(limit):
            id = self.createId()
            ids.append(id)
        return ids

