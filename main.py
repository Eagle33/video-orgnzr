#!/usr/bin/python

#-----------------------------------------------------------------------------------------------------
# Copyright 2020 by BMR All rights reserved.
# Author:   Brian Martinez 
# Date Created: May 2020
#
# Description: 
#
#-----------------------------------------------------------------------------------------------------

import common.utilities as util
from DatabaseController import DatabaseController


consoleName = r''' __      __  _       _                     ____                                      
 \ \    / / (_)     | |                   / __ \                                     
  \ \  / /   _    __| |   ___    ___     | |  | |  _ __    __ _   _ __    ____  _ __ 
   \ \/ /   | |  / _` |  / _ \  / _ \    | |  | | | '__|  / _` | | '_ \  |_  / | '__|
    \  /    | | | (_| | |  __/ | (_) |   | |__| | | |    | (_| | | | | |  / /  | |   
     \/     |_|  \__,_|  \___|  \___/     \____/  |_|     \__, | |_| |_| /___| |_|   
                                                           __/ |                     
                                                          |___/                      
'''

def main ():
    print(consoleName)
    print("*" * 80)
    print("\nStart process of adding videos to database?\n")
    print("Continue press (1)")
    print("Exit press (2) \n")

    userInput = input("Option -> ")

    if userInput == "1":
        dbConn = DatabaseController()
        status = dbConn.addVideos()
        if status == True:
            print("Success! Videos were correctly added to the database.")
        else:
            print("Fail! All videos were not succesfully added to the database")
    else:
        print("Exit video organzr.")
        
main()